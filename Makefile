
TARGET = build/bootless.a

NASM = nasm -felf64 -i $(ARCH_PATH)

X86_64_SOURCES = boot.asm structures.asm
X86_64_DEPS = print.mac defs.mac

SOURCES = $(X86_64_SOURCES)
ARCH_PATH = src/
AR = ar 

OBJECTS:=$(patsubst %,build/%.o,$(basename $(SOURCES)))
SOURCES:=$(patsubst %,$(ARCH_PATH)/%,$(X86_64_SOURCES))
ASM_DEPS:=$(patsubst %,$(ARCH_PATH)/%,$(X86_64_DEPS))

all: $(SOURCES) $(TARGET)

release: $(RELEASE_TARGET)

build: 
	mkdir -p build

$(TARGET): $(OBJECTS)
	$(AR) rcs $@ $^

$(OBJECTS): | build

build/%.o: $(ARCH_PATH)/%.asm $(ASM_DEPS)
	$(NASM) $< -o $@

clean:
	rm -f build/*.o
	rm -f build/*.a

distclean:
	rm -rf build

FORCE: ;
