## bootless

`bootless` is a simple x86 bootloader designed to take the computer from 32-bit
mode all the way to 64-bit mode. This bootloader depends on a multiboot2
compatible bootloader, such as grub, to get the computer into 32-bit
mode--muchless was built to simplify getting from grub into your kernel.

The makefile will produce a static library that you can link to your kernel.
Your kernel entry point will be a function called `kmain`, and you must define
this function to get linking to work. For an example of a project using bootless
check out https://gitlab.com/waylon531/muchless or https://gitlab.com/wayless/wayless
