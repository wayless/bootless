;
;  bootless, a simple bootloader
;  Copyright (C) Waylon Cude 2019
;
;  This Source Code Form is subject to the terms of the Mozilla Public
;  License, v. 2.0. If a copy of the MPL was not distributed with this
;  file, You can obtain one at http://mozilla.org/MPL/2.0/.
;

;Import macros to make printing to serial easier
%include "print.mac"
%include "defs.mac"

global _start
global halt
global syscall_stack_top
global registers, registers_end
global user_cr3
;global heap_bottom,heap_top
global switch_stack

extern kmain
extern gdtr
extern idtr
extern gdtr64,gdtr64_upper
extern idt64_start
extern pml4
extern idtr64
extern tss_descriptor
extern tss
extern init_timer

global start
;KERNEL_BASE equ 0xFFFF800000000000
start equ (_start-KERNEL_BASE)

global _main
;This symbol makes debugging easier
_main equ (main-KERNEL_BASE)

section .text
[bits 32] ; Computer starts in 32-bit mode
_start:
    ; set the stack up
    mov esp,stack_top-KERNEL_BASE
    ; save multiboot stuff
    push 0
    push eax
    push 0
    push ebx
    
    ; turn off interrupts
    cli
    outs 'Interrupts disabled'
    ; load gdt
    lgdt [gdtr]

    ; set the first bit of cr0, this enables protected mode
    mov eax, cr0
    or eax, 1
    mov cr0, eax

    outs 'Protected mode enabled'
    

    ;setup segmentation registers
    ; I'm actually not sure if I need to do this
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax

    outs 'Jumping to protected mode'

    ; Enter protected mode
    jmp 0x08:protected-KERNEL_BASE
    ;push 0x08
    ;push protected-KERNEL_START
    ;jmp far [esp]

protected:
    outs 'Entered protected mode'
    ; load idt
    lidt [idtr]
    outs 'IDT loaded'

    mov eax, pml4 ; set cr3, the page table base address register
    mov cr3, eax ; to point to our pdt

    mov eax, cr4 
    or eax, 1 << 5 ;Turn PAE on
    mov cr4, eax

    ; Set MSR to enable long mode
    mov ecx, 0xC0000080
    rdmsr
    or eax, 1 << 8
    wrmsr

    outs 'Long mode enabled'

    mov eax, cr0 
    or eax, 1 << 31 | 1     ;Turn paging on
    or eax, 1 << 1          ;Turn SSE stuff on
    and ax, 0xFFFB          ;More SSE stuff
    mov cr0, eax

    mov eax, cr4
    or eax, 1 << 9 | 1 << 10;Turn more SSE stuff on
    mov cr4, eax




    ; Load the gdt for long mode
    lgdt [gdtr64]


    jmp 0x8:_main
    ;push 0x08
    ;push main-KERNEL_START
    ;jmp far [esp]


[BITS 64]
main:

    outs "Entered 64-bit mode"
    mov rax,bootinfo.interrupts
    mov rbx, idt64_start
    mov qword [rax], rbx
    mov rax,bootinfo.pml4
    mov qword [rax], pml4
    
    ; Set the tss descriptor in the gdt up
    ; Unfortunately I can't do this in the preprocessor
    ; because we don't know where the label is and we can't
    ; do binary arithmetic on it
    mov rsi, tss_descriptor
    mov rcx, tss
    and ecx, 0xFFFF
    mov [rsi + 2], cx 

    mov rcx, tss
    shr ecx, 16
    and ecx, 0xFF
    mov [rsi + 4], cl

    mov rcx, tss
    shr ecx, 24
    and ecx, 0xFF
    mov [rsi + 7], cl

    mov rcx, tss
    shr rcx, 32
    and ecx, 0xFFFFFFFF
    mov [rsi + 8], ecx

    outs "Created TSS"
    ; And finally load our new gdt
    mov rax, gdtr64_upper
    lgdt [rax]

    ; And then load our tss
    mov cx, 0x2B
    ltr cx

    outs "Loaded TSS"

    mov rax, idtr64
    lidt [rax]
    outs 'Long mode entered'

    mov rax,bootinfo.multiboot
    pop rbx
    mov [rax],ebx
    mov rax,bootinfo.magic
    pop rbx
    mov [rax],ebx

    mov rdi, bootinfo

    ; set stack to a 4KB reserved area
    mov rsp, stack_top
    outs 'Stack set'

    ; Set up the lapic + timer for interrupts
    ; We do this in real mode because otherwise I have to map it
    ; LMAO I CANT BECAUSE YOU NEED TO CLEAR IT
    ;outs 'Setting up APIC timer'
    ;NOTE: I take a huge performance hit if this gets enabled
    ; but it seems to be fixed with kvm enabled
    ;call init_timer
    ;outs 'Set up APIC timer'

    ;Disable PIC
    mov al, 0xff
    out 0xa1, al
    out 0x21, al
    outs 'PIC disabled'
    ;outs 'Entering kinit'
    ;mov rax,kinit
    ;call rax

    outs 'Entering kmain'
    outc 10

    ; Convert all necessary pointers to reference
    ; memory in the higher half
    mov rdi, bootinfo

    ;This calls kmain
    ;we do this via an iret to change cs
    mov rax,kmain
    ;mov rbp, rsp
    ;push 0x10
    ;push rbp
    ;pushfq 
    ;push 0x08
    ;push rax
    ;iretq
    call rax

halt:
    outs 'FIN'

end:
    hlt
    jmp end


    ; shutdown
    ; NOTE: This assumes we are in QEMU
    mov al, 0
    ; set port to debug-exit port 
    mov dx, 0xF4
    ; output data
    out dx, al

    hlt


section .bss

bootinfo:
    .magic:
        RESD 1
    .multiboot:
        RESD 1
    .interrupts:
        RESQ 1
    .pml4:
        RESQ 1

registers:
    ; We want enough space to store 19 registers
    RESQ 18
registers_end:
    RESQ 19

; This stores a cr3 for the thread we're returning from
user_cr3:
    RESQ 1



; Reserve 8*4KB for the stack
alignb 4096
stack_bottom:
%rep 8
    RESB 4096
%endrep
stack_top:
;I might need buffer space
    RESB 4096


; Reserve 8*4KB for the system call stack
alignb 4096
syscall_stack_bottom:
%rep 8
    RESB 4096
%endrep
syscall_stack_top:
;I might need buffer space
    RESB 4096

; Reserve 2*4KB for the context switch stack
alignb 4096
switch_stack_bottom:
%rep 2
    RESB 4096
%endrep
switch_stack:
;I might need buffer space
    RESB 4096

; Reserve some space for the heap
;alignb 4096
;heap_bottom:
;    ; Reserve 1 MiB
;    RESB 1024*1024
;heap_top:

; vim: ft=nasm
