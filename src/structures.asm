;
;  bootless, a simple bootloader
;  Copyright (C) Waylon Cude 2019
;
;  This Source Code Form is subject to the terms of the Mozilla Public
;  License, v. 2.0. If a copy of the MPL was not distributed with this
;  file, You can obtain one at http://mozilla.org/MPL/2.0/.
;

;Import macros to make printing to serial easier
%include "print.mac"
%include "defs.mac"

global pml4
global gdtr
global idtr
global gdtr64,gdtr64_upper
global idtr64
global idt64_start
global tss_descriptor
global tss

extern syscall_stack_top

; multiboot header
section .multiboot
    dd 0xe85250d6
    dd 0
    dd 16
    dd -(0x10+0xe85250d6)

section .init
; Global descriptor table
gdt_start:
    dq 0 ; null entry
    dd 0x0000FFFF
    dw 0x9a00
    dw 0x00CF ; code
    dd 0x0000FFFF
    dw 0x9200
    dw 0x00CF ; data
 
; Pointer to GDT
gdtr:
    dw gdtr - gdt_start - 1
    dq gdt_start

; Global descriptor table for 64-bit
gdt64_start:
    dq 0 ; null entry
    dd 0
    dw 0x9a00
    db 00100000b
    db 0 ; code
    dd 0
    dw 0x9200
    dw 0 ; data
    dd 0
    dw 0xFa00
    db 00100000b
    db 0 ; usercode
    dd 0
    dw 0xF200
    dw 0 ; userdata
;    dw 0xFFFF
;    dw 0
;    db 0
;    db 0xFA
;    db 0xCF
;    db 0 ; usercode
;    dw 0xFFFF
;    dw 0
;    db 0
;    db 0xF2
;    db 0xCF
;    db 0 ; userdata

; Pointer to GDT
gdtr64:
    dw gdtr64 - gdt64_start - 1
    dq gdt64_start

; Interrup descriptor table
idt_start:
    times 256 dq 0
idtr:
    dw idtr - idt_start - 1
    dq idt_start




align 4096
pml4: 
    dq pdp + 0x3 ; + is as good as | I guess
    times 255 dq 0
    dq pdp_higher + 0x3
    times 255 dq 0

align 4096
pdp: 
    dq pd + 0x3
    times 511 dq 0

; Maybe TODO: do this dynamically based off of kernel
%assign i 0
align 4096
pd: 
    %rep 512
        dq (i * 2*1<<20) | 0x10B | 1<<7
        %assign i i+1
    %endrep

; Higher half mappings
; Maybe TODO: support more than 512 GiB
%assign i 0
align 4096
pdp_higher: 
    %rep 512 ; Identity map first 512GiB
        dq (i * (1<<30)) | 0x10B | 1<<7
        %assign i i+1
    %endrep




section .data

; Interrup descriptor table
idt64_start:
    times 512 dq 0
idtr64:
    dw idtr64 - idt64_start - 1
    dq idt64_start

 
tss:
    dd 0
    dq syscall_stack_top
    times 3 dq 0
    dq double_fault_stack
    dq page_fault_stack
    times (0x68 - 8*6 - 4) db 0

; Global descriptor table for 64-bit
gdt64_start_upper:
    dq 0 ; null entry
    dd 0
    db 0 ; base
    db 0x9a ; type
    db 00100000b ; flags, set long
    db 0 ; code
    dd 0
    dw 0x9200
    dw 0 ; data
    ;dd 0
    ;dw 0xFa00
    ;db 00100000b
    ;db 0 ; usercode
    dd 0
    dw 0xF200
    dw 0 ; userdata
    dd 0
    dw 0xFa00
    db 00100000b
    db 0 ; usercode
    ;dw 0xFFFF
    ;dw 0
    ;db 0
    ;db 0xFA
    ;db 0xCF
    ;db 0 ; usercode
    ;dw 0xFFFF
    ;dw 0
    ;db 0
    ;db 0xF2
    ;db 0xCF
    ;db 0 ; userdata
tss_descriptor:
    ; size of tss
    dw 0x67
    ; base address of tss
    ;NOTE: this is a zero because we have to manually set it up
    dw 0;dw (tss) & 0xFFFF
    db 0;db (tss>>16) & 0xFF
    ; flags
    db 0b11101001
    db 0 ; limit and some flags
    db 0;db (tss>>24) & 0xFF ; TSS entry
    dd 0; tss >> 32 && 0xFFFFFFFF
    dd 0; This should stay 0

; Pointer to GDT
gdtr64_upper:
    dw gdtr64_upper - gdt64_start_upper - 1
    dq gdt64_start_upper
    
    times 4096 db 0
double_fault_stack:
    times 4096 db 0 
page_fault_stack:
    times 1024 db 0 ; buffer space
; vim: ft=nasm
